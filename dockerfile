#just a good cuda base which plays nice with _most_ backends
FROM nvidia/cuda:11.0-runtime-ubuntu20.04

#make docker run in noninteractive mode for install stuff
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=America/New_York

#create scratch dir and install some basic things
#symlink python3/pip3 to work using just "python" and "pip"
RUN mkdir /scratch && \
    mkdir -p /data/shared && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git libcairo2-dev libgirepository1.0-dev mdbtools software-properties-common libpq-dev gcc zip wget curl bzip2 unzip locales build-essential libgl1-mesa-glx python3-pip ca-certificates libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev nano libpq-dev openssl gnupg1 gnupg2 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get update -y && \
    apt-get upgrade -y && \
    ln -s /usr/bin/pip3 /usr/bin/pip && \
    ln -s /usr/bin/python3 /usr/bin/python

#working directory
WORKDIR /scratch

#get stand survey data
RUN cd /scratch && \
    wget https://data.fs.usda.gov/research/pnw/rma/databases/PNWFIADB_CA.zip && \
    unzip PNWFIADB_CA.zip

#make sure that python knows that anything in our scratch dir should be on path
ENV PYTHONPATH=$PYTHONPATH:/scratch:/scratch/Python

RUN add-apt-repository -y ppa:ubuntugis/ubuntugis-unstable && \
    apt-get update && \
    apt-get install -y gdal-bin

COPY pip_requirements.txt /scratch/pip_requirements.txt

RUN pip install --no-cache-dir -r /scratch/pip_requirements.txt
