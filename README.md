# Yield Modeling Fun
This began years ago as part of just playing with some NASS county yield data and remotely sensed data to determine if one could model yield as a function of some remotely sensed features. It was fun and hits on a lot of different aspects of remote sensing, geospatial modeling, data science and many more. At the time, I worked primarily in the R programming language. Fast forward to have years of private sector, industry and consulting DS experience and everything is basically dockerized python. I thought it would be fun to try and recreate what I had before but using knowledge I've gained (hello testable functions, docker, and repeatable code...).

# OldR
This folder contains the rmarkdown file and the knitted PDF from the original effort. The original source code, if it existed outside of an RMarkdown script, has been lost to old age (dead harddrive). No further updates on that code will happen unless I get the urge to conduct a side-by-side comparison of the workflow in both R and Python (I won't but can tell you that Python will be much, much faster).

# Python
This is where the new code resides. The dockerfile in the home project path is used in the Python workflow. It is an amalgamation of my most used resources in a GPU enabled and ready docker container. It has far more than is needed for this simple project. Best practice would be to remove the installs not absolutely needed and clean things up. While I agree that in a production environment that would be the best thing to do, just playing around with data doesn't warrant that effort at this time.

# Running
Steps:
- Clone Repo
- Build dockerfile
- Run dockerfile
- run script that isn't made yet

## Clone Repo
```
git clone https://gitlab.com/matt.hayes/YieldModeling.git
```

## Build dockerfile
```
docker build -t yield .
```

## Run dockerfile

NOTE: this attachs your current working directory to the _/scratch_ directory. This overwrites whatever is in that directory but allows you to pass files back and forth between your container and local file system

```
docker run -it -v $PWD:/scratch yield
```

## Preliminary Data Prep
All preliminary data prep is rolled in to a single step

```
import dataprep

dataprep.prep_data()
```

This function will download state and county shapefiles, subset to a study area and mosaic, reproject and clip all data to our study area in EPSG 5070 (Albers Equal Area; CDL uses this projection).


## Extracting raster data to polygons
Now that the data is preliminarily cleaned and in the right projection, we need to extract values to our county polygons.
