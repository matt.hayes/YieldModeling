import subprocess
import geopandas
import os
import requests
from concurrent.futures import ProcessPoolExecutor
import pandas as pd
import rasterio
import rasterio.mask
from rasterio.merge import merge
from rasterio.warp import calculate_default_transform, reproject, Resampling

import numpy as np


def download_convert():
    """
    We need to obtain the data to setup a study area. Use the us census bureau dataset to
    pull county and state boundaries. Use geopandas to clip and project; after that, save the data
    as a geojson for better interoperability. Finally, delete all the intermediate steps since
    we have the code to recreate everything.
    """
    # if the data folder doesn't exist, make it
    if not os.path.exists("data"):
        os.makedirs("data")

    # download and unzip both the county and state data
    # subprocess.check_call(
    #     f"wget https://www2.census.gov/geo/tiger/TIGER2019/COUNTY/tl_2019_us_county.zip",
    #     shell=True,
    # )
    subprocess.check_call(f"unzip tl_2019_us_county.zip", shell=True)
    # subprocess.check_call(
    #     f"wget https://www2.census.gov/geo/tiger/TIGER2019/STATE/tl_2019_us_state.zip",
    #     shell=True,
    # )
    subprocess.check_call(f"unzip tl_2019_us_state.zip", shell=True)

    # read in the state/county data
    countyfile = geopandas.read_file("tl_2019_us_county.shp")
    countyfile = countyfile.to_crs("EPSG:5070")
    statefile = geopandas.read_file("tl_2019_us_state.shp")
    statefile = statefile.to_crs("EPSG:5070")

    # we only care about the following states for this
    targ_states = [
        "38",
        "46",
        "31",
        "20",
        "40",
        "27",
        "19",
        "29",
        "55",
        "17",
        "18",
        "39",
        "21",
        "47",
        "37",
        "51",
        "54",
        "26",
        "24",
        "10",
        "34",
        "42",
        "9",
        "36",
        "44",
        "25",
        "33",
        "50",
        "23",
    ]

    # here we subset out to only the state fips codes we care about
    substatefile = statefile[statefile["STATEFP"].isin(targ_states)]
    countysub = countyfile[countyfile["STATEFP"].isin(targ_states)]

    # there is some wonkiness in the county data, so we do an actual clip.
    # we do this after the above subset to speed everything up.
    countysub = geopandas.clip(countysub, substatefile)

    # write out our files as geojson
    substatefile.to_file("data/StateBoundaries.geojson", driver="GeoJSON")
    countysub.to_file("data/CountyBoundaries.geojson", driver="GeoJSON")

    # just quick check plotting code
    # fig = plt.plot()
    # substatefile.plot()
    # plot.save('jk.png')

    # list all files in the directory
    allfiles = os.listdir()

    # use list comp to return all the files with "tl" in the path; basically, all the raw census data
    delfiles = [x for x in allfiles if "tl" in x]

    # delete all the errant files we don't care about anymore
    # for delfile in delfiles:
    #     os.remove(delfile)


def download_quickstats(x):
    """
    nass has an API which we can get data. This function pull corn yield data by county since
    2008. There are ~1800 counties in the study area. Data is downloaded as a json object and
    then converted into a data frame for ease of use later
    """
    try:
        # dynamically create the path from the GEOID (x) in the form of STATEFIPSCOUNTYFIPS
        path = f"https://quickstats.nass.usda.gov/api/api_GET/?key=8DC4E243-37AB-385D-9F74-0527D31CB310&commodity_desc=CORN&year__GE=2008&statisticcat_desc=YIELD&CORN,%20GRAIN%20-%20YIELD,%20MEASURED%20IN%20BU%20/%20ACRE&county_code={x[2:5]}&state_fips_code={x[0:2]}&format=json"

        # POST request
        ret = requests.post(path)

        # Results come back as a json object
        df = pd.DataFrame(ret.json()["data"])

        # return data frame
        return df
    except:
        # TODO fix naked error handling
        return None


def par_download_quickstats(proc_list, n):
    """
    Nobody has time to sit around and wait for this to run, so, parallelize it
    Simply takes a **proc_list** which is a list of geoids and uses a process pool to run
    the requests in parallel
    """
    # context manager to help clean up our process pool and keep code clean
    with ProcessPoolExecutor(n) as ex:
        # map and get the output
        iterouts = ex.map(download_quickstats, proc_list)

    # processpoolexecutor is a generator object, rip the data out
    outs = [x for x in iterouts]

    # if we had a failure or something didn't work, we'll get none
    # TODO error handling is poor
    outs = [x for x in outs if x is not None]
    outs = pd.concat(outs)
    return outs


def download_nass_data(n=100):
    """
    download nass data for all counties in the study area
    n is the number of cores to use
    dfs is a dataframe
    """
    # read in the geojson
    countyfile = geopandas.read_file("data/CountyBoundaries.geojson")

    # return all the GEOIDs
    geoids = countyfile["GEOID"].values.tolist()

    # download all the data
    dfs = par_download_quickstats(geoids, n)

    # clean the dataframe
    dfs = dfs[
        [
            "state_alpha",
            "state_fips_code",
            "county_code",
            "year",
            "Value",
            "short_desc",
            "unit_desc",
        ]
    ]
    dfs = dfs[dfs["short_desc"] == "CORN, GRAIN - YIELD, MEASURED IN BU / ACRE"]

    # return concat'd dataframe
    return dfs


def dissolve_counties():
    """
    clipping on a single boundary is faster than a bunch of sub-geometries, dissolve to outer boundary
    """
    # read in the geojson
    countyfile = geopandas.read_file("data/CountyBoundaries.geojson")
    # create a dummy variable to dissolve on
    countyfile["n"] = 1
    # dissolve geojson by dummy column
    dissolvecounty = countyfile.dissolve(by="n")
    # return dissolved data
    return dissolvecounty


def unzip_rasters():
    """
    I manually downloaded rasters from earth explorer; my machine-machine api key
    needed to be renewed and it was taking too long. I manually downloaded and chucked them
    into a directory for processing.
    the V01 files are from the terra sensor and v02 are from the Aqua

    Here, we unzip all the data to prepare for further processing
    """
    # list all the file in the modis folder
    rasfiles = os.listdir("data/modis")

    # we only want to process v1 zip files
    rasfiles = [x for x in rasfiles if "V01.ZIP" in x]

    print(f"There are {len(rasfiles)} to unzip")

    # for each file in the list, unzip to the data/raster folder
    for rasfile in rasfiles:
        subprocess.check_call(f"unzip data/modis/{rasfile} -d data/raster")


def mosaic_clip_rasters(files_to_process="v1"):
    """
    we need to mosaic the west/east files for each metric and then clip to our study area
    """
    # grabbed dissolved study area boundary
    discount = dissolve_counties()

    # list all files in the data/raster folder
    rasfiles = os.listdir("data/raster")

    # we only care about the v1 tiff files
    rasfiles = [x for x in rasfiles if f"{files_to_process}.tif" in x]

    # this is a way to build a list that contains all the {metric}{year} combinations
    rastypes = list(set([x.split("_")[0] for x in rasfiles]))

    # rastypes needs to be iterated through. Given potential memory constraints, run in series
    for rastype in rastypes:
        # empty list to append opened data to
        rasters_to_mosaic = []

        # subset out the files that need to be mosaicked
        targras = [x for x in rasfiles if rastype in x]

        # if you open a file in QGIS (or presumably arcgis) an aux.xml file will
        # be here. Remove from our list
        # TODO: thing about if this is the best way to handle
        targras = [x for x in targras if "aux.xml" not in x]

        # should be 2 rasters (an east and a west) to mosaic
        print(targras)
        assert len(targras) == 2

        # iterate through both rasters and open
        for targ in targras:
            # open and append
            rasters_to_mosaic.append(rasterio.open(f"data/raster/{targ}"))

        # merge the list of rasters together
        mosaic, out_trans = merge(rasters_to_mosaic)

        # copy the meta data
        out_meta = rasters_to_mosaic[0].meta.copy()

        # Update the metadata
        out_meta.update(
            {
                "driver": "GTiff",
                "height": mosaic.shape[1],
                "width": mosaic.shape[2],
                "transform": out_trans,
            }
        )
        # write out the temporary file
        with rasterio.open("data/raster/temp.tif", "w", **out_meta) as dest:
            dest.write(mosaic)

        # warp and clip the mosaic to our study area
        warp_raster(
            src_file="data/raster/temp.tif",
            dst_file=f"data/mosaic/{rastype}_{files_to_process}.tif",
            vec=discount,
        )


def warp_raster(vec, src_file="data/raster/temp.tif", dst_file="data/out_reproj.tif"):
    """
    This is a workhorse function.

    Here, we take our temporary mosaicked rasters, warp to our target crs and then crop to our study area
    boundary. Finally, the file is saved out to our "mosaic" folder.

    TODO: this really should be a couple of functions, hard to test this easily
    """
    # our destination crs
    # TODO - this should be in a constants file
    dst_crs = rasterio.crs.CRS.from_epsg(5070)

    # use a context manager to open the source file, determine the transform and update the metadata
    with rasterio.open(src_file) as src:
        # determine the transform needed and save out for writing
        transform, width, height = calculate_default_transform(
            src.crs, dst_crs, src.width, src.height, *src.bounds
        )
        # copy and update the metadata
        kwargs = src.meta.copy()
        kwargs.update(
            {"crs": dst_crs, "transform": transform, "width": width, "height": height}
        )
        # reproject the temporary raster
        # NOTE: using bilinear resampling
        with rasterio.open("data/raster/temp1.tif", "w", **kwargs) as dst:
            for i in range(1, src.count + 1):
                reproject(
                    source=rasterio.band(src, i),
                    destination=rasterio.band(dst, i),
                    src_transform=src.transform,
                    src_crs=src.crs,
                    dst_transform=transform,
                    dst_crs=dst_crs,
                    resampling=Resampling.bilinear,
                )
    # open the temp file, mask and write out the final dataset
    with rasterio.open("data/raster/temp1.tif") as src:
        out_image, out_transform = rasterio.mask.mask(src, vec.geometry, crop=True)
        out_meta = src.meta
        out_meta.update(
            {
                "driver": "GTiff",
                "height": out_image.shape[1],
                "width": out_image.shape[2],
                "transform": out_transform,
            }
        )
        with rasterio.open(dst_file, "w", **out_meta) as dest:
            dest.write(out_image)


def prep_data():
    """
    wrapper to do all the work
    """
    download_convert()
    mosaic_clip_rasters("v1")
    mosaic_clip_rasters("v2")
